def main():
    gasto = float(input('Qual o valor gasto em minutos de sua conta: '))
    if gasto <= 200.:
        total = gasto * 0.20
        print('Você gastou R$ %.2f ' %total)
    elif gasto > 200. and gasto <= 400.:
        total = gasto * 0.18
        print('Você gastou R$ %.2f ' % total)
    elif gasto > 400.:
        total = gasto * 0.15
        print('Você gastou R$ %.2f ' % total)
    else:
        print('Não foi possível calcular o seu gasto telefônico.')




main()