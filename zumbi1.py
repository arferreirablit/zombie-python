a = 1
b = 2

a = 'abacate'
b = ' e banana'

print (a + b)

dir(a)

if  a == b:
    print('As strings são iguais')
else:
    print('As variáveis são diferentes!')


languages = ['Ruby', 'Python']

for language in languages:
    print('Eu programo em: ' + language)

a = 13
print ('O número é 13', a)

# Marcadores
print ('o número %d é muito legal' %a)
a = 3.1415
print('O pi vale: %f' %a)
print('o pi vale: %.2f' %a)

# variáveis tipicamente dinâmicas
a = 'mamão'
#a = 42
# tipagem forte
#42 + a
str(42) + a
# atribuição multipla
a, b = b, a

# teste de mesa ou simulação
divida = 0
compra = 42
divida = divida + compra
compra = 13
divida = divida + compra
compra = 50
divida = divida + compra
print(divida)